# ic-office-xml-fix

Fixes rounding errors in invoices exported from IC Office.

## Manual Commands

Use following command:
```shell
xsltproc app/transform.xslt broken.xml >fixed.xml
```

Or for pretty-printed XML:
```shell
xsltproc app/transform.xslt broken.xml | xmllint --format - >fixed.xml
```

Sum SouhrnDPH.Zaklad for all Polozka elements (useful for debugging):
```shell
cat broken.xml \
    | yq -p=xml '.MoneyData.SeznamFaktVyd.FaktVyd.SeznamPolozek.Polozka[].SouhrnDPH.Zaklad' \
    | paste -sd+ - \
    | bc
```

## Web Interface

Use docker compose to run web interface service:

```shell
docker-compose up
```

The service is a simple go application that uses go-xslt to fix the XML.
