<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:output method="xml" encoding="utf-8"/>

    <!-- copy everything as-is (identity template) -->
    <!-- see http://www.javabyexamples.com/xslt-identity-template -->
    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="SeznamFaktVyd/FaktVyd/Popis/text()">
        <xsl:text>Oprava vozu a ND</xsl:text>
    </xsl:template>

    <xsl:template match="SeznamFaktVyd/FaktVyd/Celkem/text()">
        <xsl:call-template name="sum">
            <xsl:with-param name="nodes" select="../../SeznamPolozek/Polozka"/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="SeznamFaktVyd/FaktVyd/SouhrnDPH/Zaklad22/text()">
        <xsl:value-of select="sum(../../../SeznamPolozek/Polozka/SouhrnDPH/Zaklad/text())"/>
    </xsl:template>

    <xsl:template match="SeznamFaktVyd/FaktVyd/SouhrnDPH/DPH22/text()">
        <xsl:value-of select="sum(../../../SeznamPolozek/Polozka/SouhrnDPH/DPH/text())"/>
    </xsl:template>

    <xsl:template name="sum">
        <xsl:param name="nodes"/>
        <xsl:param name="sum" select="0"/>

        <xsl:variable name="node" select="$nodes[1]"/>

        <!-- if we have a node, calculate & recurse -->
        <xsl:if test="$node">
            <xsl:variable name="running-sum" select="$sum + $node/PocetMJ * $node/Cena"/>
            <xsl:call-template name="sum">
                <xsl:with-param name="nodes" select="$nodes[position() &gt; 1]"/>
                <xsl:with-param name="sum" select="$running-sum"/>
            </xsl:call-template>
        </xsl:if>

        <!-- if we don't have a node (last recursive step), return sum -->
        <xsl:if test="not($node)">
            <xsl:value-of select="format-number($sum, '0.##')"/>
        </xsl:if>
    </xsl:template>

</xsl:stylesheet>
