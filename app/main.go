package main

import (
	"fmt"
	"github.com/wamuir/go-xslt"
	"io"
	"net/http"
	"net/url"
	"os"
)

func main() {
	http.Handle("/", http.FileServer(http.Dir("./static")))
	http.HandleFunc("/upload", uploadFile)
	http.ListenAndServe(":8080", nil)
}

func uploadFile(w http.ResponseWriter, r *http.Request) {
	// Parse input
	r.ParseMultipartForm(10 << 20) // 10 MB max

	// Get uploaded file
	file, header, err := r.FormFile("xml-file")
	if err != nil {
		http.Error(w, "Failed to retrieve uploaded file", http.StatusBadRequest)
		return
	}
	defer file.Close()

	// Read uploaded XML file into memory
	xmlData, err := io.ReadAll(file)
	if err != nil {
		http.Error(w, "Failed to read uploaded file", http.StatusInternalServerError)
		return
	}

	// Perform XSLT transformation
	output, err := transformXML(xmlData, "transform.xslt")
	if err != nil {
		http.Error(w, "Failed to perform XSLT transformation: "+err.Error(), http.StatusInternalServerError)
		return
	}

	contentDisposition := fmt.Sprintf(
		"attachment; filename*=utf-8''%s",
		url.QueryEscape(
			fmt.Sprintf("fixed-%s", header.Filename),
		),
	)

	// Write the transformed XML to the response
	w.Header().Set("Content-Disposition", contentDisposition)
	w.Header().Set("Content-Type", "application/xml")
	w.Write(output)
}

func transformXML(xmlData []byte, xslFile string) ([]byte, error) {
	// Parse XSLT file
	xslData, err := os.ReadFile(xslFile)
	if err != nil {
		return nil, err
	}

	stylesheet, err := xslt.NewStylesheet(xslData)
	if err != nil {
		return nil, err
	}
	defer stylesheet.Close()

	transformed, err := stylesheet.Transform(xmlData)
	if err != nil {
		return nil, err
	}

	return transformed, nil
}
